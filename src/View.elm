module View exposing (view)

import Types exposing (..)
import Html exposing (Html, Attribute, a, h4, div, text, table, tbody, tr, td)
import Html.Attributes exposing (..)


view : Model -> Html Msg
view model =
    div [ class "inner columns aligned" ] <| List.map viewMassageDay model.days


viewMassageDay : MassageDay -> Html Msg
viewMassageDay day =
    div [ class "span-4" ]
        [ h4 []
            [ text <| day.date ++ " / Location: " ++ day.location ]
        , div [ class "table-wrapper" ]
            [ table [ class "alt" ]
                [ tbody [] <| List.map viewMassageHour day.hours
                ]
            ]
        ]


viewMassageHour : MassageHour -> Html Msg
viewMassageHour hour =
    tr [] <| List.concatMap viewMassageSlot hour.slots


viewMassageSlot : MassageSlot -> List (Html Msg)
viewMassageSlot slot =
    [ td [] [ text slot.time ]
    , td [] [ viewBooking slot.booking ]
    ]


viewBooking : Booking -> Html Msg
viewBooking booking =
    case booking of
        NotFound ->
            text "-------"

        Found _ url Nothing True ->
            a [ href url ] [ text "OPEN" ]

        Found _ url Nothing False ->
            a [ href url ] [ text "CLOSED" ]

        Found _ url (Just name) _ ->
            a [ href url ] [ text name ]
