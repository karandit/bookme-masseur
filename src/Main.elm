module Main exposing (..)

import Types exposing (..)
import API
import View exposing (view)
import Dict
import Http
import LinkHeader
import Html


main =
    Html.program { init = init, view = view, update = update, subscriptions = \_ -> Sub.none }



-- MODEL ---------------------------------------------------------------------------------------------------------------


init : ( Model, Cmd Msg )
init =
    { days = []
    }
        ! [ loadDb ]


loadDb : Cmd Msg
loadDb =
    Http.send LoadDays API.getDays


loadFirst100GitlabIssues : Cmd Msg
loadFirst100GitlabIssues =
    Http.send LoadGitlabIssues API.getFirstGitlabIssues


loadGitlabIssues : String -> Cmd Msg
loadGitlabIssues =
    Http.send LoadGitlabIssues << API.getGitlabIssues


loadGitlabNotes : Int -> Cmd Msg
loadGitlabNotes issueId =
    Http.send LoadGitlabNotes <| API.getGitlabNotes issueId



-- UPDATE --------------------------------------------------------------------------------------------------------------


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LoadDays (Ok days) ->
            { model | days = List.reverse days } ! [ loadFirst100GitlabIssues ]

        LoadDays (Err err) ->
            (alert model err "Couldn't load db.json") ! []

        LoadGitlabIssues (Ok resp) ->
            let
                nextLinkCmds =
                    Dict.get "Link"
                        >> Maybe.andThen (LinkHeader.parse >> List.filter LinkHeader.webLinkIsNext >> List.head)
                        >> Maybe.map (.url >> loadGitlabIssues >> List.singleton)
                        >> Maybe.withDefault []

                ( newModel, getNoteCmds ) =
                    updateModelWithIssues resp.body model
            in
                newModel ! ((nextLinkCmds resp.headers) ++ getNoteCmds)

        LoadGitlabIssues (Err err) ->
            (alert model err "Couldn't load gitlab issues") ! []

        LoadGitlabNotes (Ok notes) ->
            let
                newModel =
                    notes
                        |> List.head
                        |> Maybe.map (updateModelWithNote model)
                        |> Maybe.withDefault model
            in
                newModel ! []

        LoadGitlabNotes (Err err) ->
            (alert model err "Couldn't load gitlab note ") ! []


updateModelWithIssues : List GitlabIssue -> Model -> ( Model, List (Cmd Msg) )
updateModelWithIssues issues model =
    let
        map_of_issues =
            Dict.fromList <| List.map (\i -> ( i.title, i )) issues

        mergeDay day =
            { day | hours = List.map (mergeHour day.date) day.hours }

        mergeHour date hour =
            { hour | slots = List.map (mergeSlot date) hour.slots }

        mergeSlot date slot =
            { slot | booking = updateBooking slot.booking date slot.time }

        updateBooking booking date time =
            Dict.get (date ++ " " ++ time) map_of_issues
                |> Maybe.map (\issue -> Found issue.iid issue.webUrl Nothing (issue.state == "opened"))
                |> Maybe.withDefault booking

        getNoteCmds =
            List.concatMap getNoteCmdsByDay model.days

        getNoteCmdsByDay day =
            List.concatMap (getNoteCmdsByHour day.date) day.hours

        getNoteCmdsByHour date hour =
            List.concatMap (getNoteCmdsBySlot date) hour.slots

        getNoteCmdsBySlot date slot =
            Dict.get (date ++ " " ++ slot.time) map_of_issues
                |> Maybe.map
                    (\issue ->
                        if (issue.notesCount > 0) then
                            [ loadGitlabNotes issue.iid ]
                        else
                            []
                    )
                |> Maybe.withDefault []
    in
        ( { model | days = List.map mergeDay model.days }
        , getNoteCmds
        )


updateModelWithNote : Model -> GitlabNote -> Model
updateModelWithNote model note =
    let
        updateDayWithNote day =
            { day | hours = List.map updateHourWithNote day.hours }

        updateHourWithNote hour =
            { hour | slots = List.map updateSlotWithNote hour.slots }

        updateSlotWithNote slot =
            { slot | booking = updateBookingWithNote slot.booking }

        updateBookingWithNote booking =
            case booking of
                Found iid url _ state ->
                    if iid == note.iid then
                        Found note.iid url (Just note.author.name) state
                    else
                        booking

                _ ->
                    booking
    in
        { model | days = List.map updateDayWithNote model.days }


alert : a -> Http.Error -> String -> a
alert x err message =
    Debug.log ("Error: " ++ message ++ " " ++ toString err) x
