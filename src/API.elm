module API
    exposing
        ( getDays
        , getGitlabIssues
        , getFirstGitlabIssues
        , getGitlabNotes
        )

import Types exposing (..)
import Json.Decode as Json exposing (..)
import Http
import Http.Extra


-- API end-points ------------------------------------------------------------------------------------------------------


db_url : String
db_url =
    "https://karandit.gitlab.io/masseur/db.json"


issues_url : String
issues_url =
    "https://gitlab.com/api/v4/projects/5582750/issues"


issues_url_created_by_admin : String
issues_url_created_by_admin =
    issues_url ++ "?author_id=357102&per_page=100"


notes_url : Int -> String
notes_url issueId =
    issues_url ++ "/" ++ (toString issueId) ++ "/notes?sort=asc&order_by=created_at&per_page=1"


headers : List HeaderFlag
headers =
    [ HeaderFlag "Private-Token" "PMApxGsoczHk8TE737Bx" ]


getDays : Http.Request (List MassageDay)
getDays =
    Http.get db_url decodeDays


getGitlabIssues : String -> Http.Request (Http.Response (List GitlabIssue))
getGitlabIssues url =
    getWithResponse headers url decodeGitlabIssues


getFirstGitlabIssues : Http.Request (Http.Response (List GitlabIssue))
getFirstGitlabIssues =
    getGitlabIssues issues_url_created_by_admin


getGitlabNotes : Int -> Http.Request (List GitlabNote)
getGitlabNotes issueId =
    get headers (notes_url issueId) decodeGitlabNotes


get : List HeaderFlag -> String -> Decoder a -> Http.Request a
get headers url decoder =
    Http.request
        { method = "GET"
        , headers = List.map (\h -> Http.header h.key h.value) headers
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectJson decoder
        , timeout = Nothing
        , withCredentials = False
        }


getWithResponse : List HeaderFlag -> String -> Decoder a -> Http.Request (Http.Response a)
getWithResponse headers url decoder =
    Http.request
        { method = "GET"
        , headers = List.map (\h -> Http.header h.key h.value) headers
        , url = url
        , body = Http.emptyBody
        , expect = Http.Extra.expectJsonResponse decoder
        , timeout = Nothing
        , withCredentials = False
        }



-- Json decoders/encoders ----------------------------------------------------------------------------------------------


decodeDays : Decoder (List MassageDay)
decodeDays =
    Json.field "days" <|
        Json.list <|
            Json.map3 MassageDay
                (Json.field "date" Json.string)
                (Json.field "location" Json.string)
                (Json.field "hours" <|
                    Json.list
                        (Json.map MassageHour
                            (Json.field "slots" <|
                                Json.list
                                    (Json.map createSlot Json.string)
                            )
                        )
                )


createSlot : String -> MassageSlot
createSlot title =
    MassageSlot title NotFound


decodeGitlabIssues : Decoder (List GitlabIssue)
decodeGitlabIssues =
    Json.list <|
        Json.map5 GitlabIssue
            (Json.field "iid" Json.int)
            (Json.field "state" Json.string)
            (Json.field "title" Json.string)
            (Json.field "user_notes_count" Json.int)
            (Json.field "web_url" Json.string)


decodeGitlabNotes : Decoder (List GitlabNote)
decodeGitlabNotes =
    Json.list <|
        Json.map2 GitlabNote
            (Json.field "noteable_iid" Json.int)
            (Json.field "author" <|
                Json.map2 GitlabUser
                    (Json.field "name" Json.string)
                    (Json.field "avatar_url" Json.string)
            )
