module Types exposing (..)

import Http


type alias HeaderFlag =
    { key : String
    , value : String
    }


type alias Model =
    { days : List MassageDay
    }


type alias MassageDay =
    { date : String
    , location : String
    , hours : List MassageHour
    }


type alias MassageHour =
    { slots : List MassageSlot
    }


type alias MassageSlot =
    { time : String
    , booking : Booking
    }


type Booking
    = NotFound
    | Found Int String (Maybe String) Bool


type alias GitlabIssue =
    { iid : Int
    , state :
        String
        -- TODO change to Bool
    , title : String
    , notesCount : Int
    , webUrl : String
    }


type alias GitlabNote =
    { iid : Int
    , author : GitlabUser
    }


type alias GitlabUser =
    { name : String
    , avatar_url : String
    }


type Msg
    = LoadDays (Result Http.Error (List MassageDay))
    | LoadGitlabIssues (Result Http.Error (Http.Response (List GitlabIssue)))
    | LoadGitlabNotes (Result Http.Error (List GitlabNote))
